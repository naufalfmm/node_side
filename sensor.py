from gpiozero import DistanceSensor
import config
import time

#start condition
def no_object(min_dist, max_dist):
    found_time = 0
    print(ultrasonic1, ultrasonic2)
    obj_dist = round(ultrasonic1.distance, 2)
    obj_dist = round((0.9959 * obj_dist + 0.0124), 2)
    print(obj_dist, min_dist, max_dist)

    while obj_dist < min_dist or obj_dist > max_dist:
        print("No Object")
        obj_dist = round(ultrasonic1.distance, 2)
        obj_dist = round((0.9959 * obj_dist + 0.0124), 2)
        print(obj_dist, min_dist, max_dist)
        time.sleep(0.001)

    found_time = time.time()
    return [found_time, obj_dist]

#second condition
def object_found(dist, timeout=60):
    come_time = 0
    while_start = time.time()
    obj_dist = round(ultrasonic2.distance, 2)
    obj_dist = round((1.0067 * obj_dist + 0.0067), 2)
    print(obj_dist, dist)
    #while obj_dist != dist:
    while obj_dist < dist - 0.1 or obj_dist > dist + 0.1:
        print("Not Come Yet")
        obj_dist = round(ultrasonic2.distance, 2)
        obj_dist = round((1.0067 * obj_dist + 0.0067), 2)
        print(obj_dist, dist)
        print(while_start + timeout > time.time())
        if while_start + timeout <= time.time():
            return [False, 0]
            break

        time.sleep(0.001)

    come_time = time.time()
    return [True, come_time]

#third condition
def till_end(dist):
    end_time = time.time()
    obj_dist = round(ultrasonic1.distance, 2)
    obj_dist = round((0.9959 * obj_dist + 0.0124), 2)
    while obj_dist == dist:
        obj_dist = round(ultrasonic1.distance, 2)
        obj_dist = round((0.9959 * obj_dist + 0.0124), 2)
        time.sleep(0.001)
    end_time = time.time()
    return end_time

#calculate the speed and dimension of vehicle based on 3 noted time: found time, come time, and end time
def veh_dimension(found_time, come_time, end_time):
    speed = 0
    sensor_dist = 0.31
    speed = sensor_dist / (come_time - found_time)
    length = sensor_dist + (speed * (end_time - come_time))

    return [speed, length]

#classify the vehicle by length into 3 classification (according to PKJI): motorcycle, light vehicle, and heavy vehicle
def veh_class(length):
    if length <= 2.5:
        return 0  # motorcycle
    elif length <= 5.5:
        return 1  # light vehicle
    else:
        return 2  # heavy vehicle


def road_status(mc, lv, hv):  # according to PKJI (2014)
    sat_deg = 0
    traf_flow = mc + lv + hv
    lane_cap = config.lane_cap/4
    if config.road_type == 0:  # 2/2TT
        if config.lane_width <= 6:
            if traf_flow >= 925:
                sat_deg = (1.3 * hv + lv + 0.5 * mc) / lane_cap
            else:
                sat_deg = (1.2 * hv + lv + 0.35 * mc) / lane_cap
        else:
            if traf_flow >= 925:
                sat_deg = (1.3 * hv + lv + 0.4 * mc) / lane_cap
            else:
                sat_deg = (1.2 * hv + lv + 0.25 * mc) / lane_cap
    elif config.road_type == 1:  # 2/1 dan 4/2T
        if traf_flow < 263:
            sat_deg = (1.3 * hv + lv + 0.4 * mc) / lane_cap
        else:
            sat_deg = (1.2 * hv + lv + 0.25 * mc) / lane_cap
    else:  # 3/1 dan 6/2T
        if traf_flow < 275:
            sat_deg = (1.3 * hv + lv + 0.4 * mc) / lane_cap
        else:
            sat_deg = (1.2 * hv + lv + 0.25 * mc) / lane_cap

    if sat_deg <= 0.19:
        stat = "A"
    elif sat_deg <= 0.44:
        stat = "B"
    elif sat_deg <= 0.74:
        stat = "C"
    elif sat_deg <= 0.84:
        stat = "D"
    elif sat_deg <= 1.00:
        stat = "E"
    else:
        stat = "F"

    return stat

def qty_speed(veh_data):
    mc_qty = 0
    lv_qty = 0
    hv_qty = 0
    mc_spd = 0.0
    lv_spd = 0.0
    hv_spd = 0.0

    for i in range(len(veh_data)):
        if veh_data[0][0] == 0:
            mc_qty += 1
            mc_spd += veh_data[0][1]
        elif veh_data[0][0] == 1:
            lv_qty += 1
            lv_spd += veh_data[0][1]
        else:
            hv_qty += 1
            hv_spd += veh_data[0][1]

        # print(mc_qty, lv_qty, hv_qty, mc_spd, lv_spd, hv_spd)

        veh_data.pop(0)

    if mc_qty != 0: 
        mc_spd = mc_spd / mc_qty
    if lv_qty != 0:
        lv_spd = lv_spd / lv_qty
    if hv_qty != 0:
        hv_spd = hv_spd / hv_qty

    return [mc_qty, lv_qty, hv_qty, mc_spd, lv_spd, hv_spd]

def start():
    global ultrasonic1, ultrasonic2
    ultrasonic2 = DistanceSensor(echo=14, trigger=15, max_distance=7)
    ultrasonic1 = DistanceSensor(echo=20, trigger=21, max_distance=7)
