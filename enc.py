from Crypto.Cipher import AES as aes
import hashlib as hs
import base64
import config

def encrypt_aes(message):
    print(len(config.key),config.key)
    obj = aes.new(config.key,1)
    print(0)
    mes_pad = message + ((16 - len(message) % 16) * chr(16 - len(message) % 16)) #PKCS7 Padding System
    print(1)
    ciph = obj.encrypt(mes_pad)
    print(2)
    return base64.b64encode(ciph)

def decrypt_aes(cipher64):
    obj = aes.new(config.key,1)
    ciph = base64.b64decode(cipher64)
    mess = obj.decrypt(ciph)
    pos = ord(mess[-1])
    mess = mess[:-pos]
    return mess

def encrypt_sha512(message):
    obj = hs.sha512()
    obj.update(message.encode('utf-8'))
    return obj.hexdigest()