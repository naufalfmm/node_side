import requests
import config
import conn

headerauth = []

def login():
    print("LOGIN")
    mess = {'username':config.username}
    url = conn.url + "device/login"
    print(url)
    try:
        r = requests.post(url, data=mess, timeout=conn.timeout)
        print(r)
    except requests.exceptions.RequestException as e:
        return False
    if r.json()["status"]:
        headerauth.append(r.json()["token"])
        print(headerauth)
        print("LOGIN SUCCESS")
        print(r.json()["message"])
    return r.json()["status"]

def logout():
    headerauth.pop(0)
    print("Good Bye! Have a Nice Day!")
    return
