import requests
import config
import enc
import auth
# from multiprocessing import Manager

url = "http://localhost:7777/api/"
server_data = []
timeout = 300

# def device_login(url = url, data = {"username":config.username}, timeout = 30):
#     try:
#         r = requests.post(url, data = data, timeout=timeout)
#     except requests.exceptions.RequestException as e:
#         return [408,{'status':False, 'message': "Request Timeout"}]
#     return [r.status_code, r.json()]

# def check_conn(url = url, timeout = 2):
#     try:
#         r = requests.head(url, timeout = timeout)
#     except requests.exceptions.RequestException as e:
#         return False
#     return r.status_code == 200 or r.status_code == 201

def post_to_server(url, timeout = timeout, data = {}, header = {}):
    try:
        r = requests.post(url, data = data, headers = header, timeout = timeout)
        json_data = r.json()
        print(json_data)
        if json_data['status'] == False and json_data['message'] == 'Unauthorized':
            #raise Exception(False, False, "Unauthorized")
            return [False,False,"Unauthorized"]
    except requests.exceptions.RequestException as e:
        return [False, True, "Request Timeout"]
    return [r.json()["status"], True, r.json()["message"]]


def post_func(header_post):
    send_data = {"data": server_data[0:]}
    stat_post = post_to_server(url=url + "data/post", data=send_data, header={"auth":header_post})
    if stat_post[0] == True:
        del server_data[:]
        return
    return

# def second_process(header_post):
    # while True:
    #     if len(server_data) > 0:
    #         send_data = change_to_json(server_data[0])
    #         try:
    #             stat_post = post_to_server(
    #                 url=url + "data/post", data=send_data, header=header_post)
    #             if stat_post[0] == True:
    #                 server_data.pop(0)
    #         except BaseException as exc:
    #             raise Exception(exc.args[0], exc.args[1], exc.args[2])

'''
def second_process():
    print("SECOND PROCESS")
    while True:
        if len(auth.headerauth) == 0:
            while not auth.login():
                pass
        # print("0 SECOND PROCESS", len(server_data), server_data)
        if len(server_data) > 0:
            print("MORE THAN ZERO")
            sent_data = change_to_json(server_data[0])
            # print("1 SECOND PROCESS")
            print(auth.headerauth[0])
            try:
                stat_post = post_to_server(
                    url=url + "data/post", data=sent_data, header={"auth":auth.headerauth[0]})
                if stat_post[0] == True:
                    server_data.pop(0)
            except BaseException as exec:
                print("BASE EXCEPTION", exec)
                if exec.__class__.__name__ == "Exception":
                    if exec.args[1] == False:
                        auth.headerauth.pop(0)
                        # second_process()
'''