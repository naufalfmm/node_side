import conn
import config
import time
import sensor
import random
import enc
# from multiprocessing import Process,Manager,Event,freeze_support

veh_data = []
maxdist = 1.8
mindist = 0.5
# server_data = []


def get_data(veh_data):
    print("GET DATA")
    found_time, obj_dist = sensor.no_object(mindist, maxdist)
    print(obj_dist, found_time)
    find, come_time = sensor.object_found(obj_dist)
    print(find, come_time)
    if find:
        end_time = sensor.till_end(obj_dist)
        print(end_time)
        speed, length = sensor.veh_dimension(found_time, come_time, end_time)
        print(found_time, come_time, end_time)
        print("Kecepatan =", speed, "Panjang =", length)
        classf = sensor.veh_class(length)
        print("Class =", classf)
        veh_data.append([classf, speed])
    return


def get_data_loop(veh_data, e):
    while True:
        found_time, obj_dist = sensor.no_object(mindist, maxdist)
        come_time = sensor.object_found(obj_dist)
        end_time = sensor.till_end(obj_dist)
        speed, length = sensor.veh_dimension(found_time, come_time, end_time)
        print(found_time, come_time, end_time)
        print("Kecepatan =", speed, "Panjang =", length)
        classf = sensor.veh_class(length)
        print("Class =", classf)
        veh_data.append([classf, speed])
        if e.is_set():
            break

'''
def get_data(veh_data):
    exist = random.randint(0, 2)
    if (exist == 1):
        veh_type = random.randint(0, 2)
        veh_speed = round(random.uniform(10, 50), 2)
        print("get_data", veh_type, veh_speed)
        veh_data.append([veh_type, veh_speed])
    time.sleep(1)

def get_data_loop(veh_data, e):
    print("GET DATA LOOP")
    while True:
        exist = random.randint(0, 2)
        if (exist == 1):
            veh_type = random.randint(0, 2)
            veh_speed = round(random.uniform(10, 50), 2)
            print("get_data", veh_type, veh_speed)
            veh_data.append([veh_type, veh_speed])
        time.sleep(1)
        if e.is_set():
            print("END GET DATA LOOP")
            break
'''


def change_to_json(data):
    json_string = '{"mtcqty":' + str(data[0]) + ',\n"lvqty":' + str(data[1]) + ',\n"hvqty":' + str(data[2]) + ',\n"mtcms":' + str(
        data[3]) + ',\n"lvms":' + str(data[4]) + ',\n"hvms":' + str(data[5]) + ',\n"status":' + '"' + str(data[6]) + '",\n"time":' + str(data[7]) + '}'
    enc_json = enc.encrypt_aes(json_string)
    check = enc.encrypt_sha512(json_string)
    send_data = {"data": enc_json, "check": check}
    return send_data


def into_data(veh_data, server_data):
    print("INTO DATA")
    data = sensor.qty_speed(veh_data)[0:]
    stat = sensor.road_status(data[0], data[1], data[2])
    data.append(stat)
    data.append(time.time())
    server_data.append(change_to_json(data))
    print(server_data)
    return
    # e.set()

'''
def first_process(server_data = conn.server_data):
    print("FIRST PROCESS")
    start_time = time.time()
    veh_data = Manager().list()
    try:
        while True:
            print(start_time)
            if start_time + 900 >= time.time():
                get_data(veh_data)
            else:
                # print("ELSE")
                e = Event()
                p1 = Process(target=get_data_loop, args=(veh_data, e))
                p2 = Process(target=into_data, args=(veh_data, server_data, e))
                p1.start()
                p2.start()
                p1.join()
                p2.join()
                start_time = time.time()
    except KeyboardInterrupt:
        p1.join()
        p2.join()
'''
