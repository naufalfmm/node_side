import conn
import auth
import sensor
import data
import time

def main():
    while not auth.login():
        pass
    data.maxdist = float(input("Masukkan jarak maksimal sensor (m): "))
    data.mindist = float(input("Masukkan jarak minimum sensor (m): "))
    try:
        i = 0
        sensor.start()
        start_time = time.time()
        while True:
            if start_time + 900 >= time.time():
                data.get_data(data.veh_data)
            else:
                data.into_data(data.veh_data, conn.server_data)
                conn.post_func(auth.headerauth)
                i += 1
                start_time = time.time()
    except BaseException as exec:
        print("Error", exec)


'''
def main():
    data.maxdist = input("Masukkan jarak maksimal sensor (m): ")
    data.mindist = input("Masukkan jarak minimum sensor (m): ")
    try:
        sensor.start()
        # print(conn.server_data)
        freeze_support()
        p1 = Process(target = data.first_process, args=())
        p2 = Process(target = conn.second_process, args=())
        p1.start()
        p2.start()
        p1.join()
        p2.join()
    except KeyboardInterrupt:
        # print("SERVER DATA=",len(conn.server_data))
        p1.join()
        p2.join()
        auth.logout()
'''

if __name__ == '__main__':
    main()
    
